import random
import time
import unittest

from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

import page
from locators import MainPageLocators
from selenium import webdriver


class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        options = webdriver.ChromeOptions()
        options.add_argument('--ignore-ssl-errors=yes')
        options.add_argument('--ignore-certificate-errors')
        options.add_argument("--no-sandbox")
        # options.add_argument("--headless")   # disable view
        options.add_argument("--disable-gpu")
        options.add_argument("--window-size=1920,1080")
        self.driver = webdriver.Chrome(
            service=Service(ChromeDriverManager().install()),
            options=options
        )
        self.driver.maximize_window()
        self.driver.get("https://localhost/")

    def test_shop(self):
        main_page = page.MainPage(self.driver)
        self.assertTrue(main_page.is_title_matches(), "ebusters title doesn't match.")
        self.add_products_to_cart(main_page)
        time.sleep(1)
        self.delete_product_from_cart(main_page)
        time.sleep(1)
        self.register_user(main_page)
        self.set_address(main_page)
        time.sleep(1)
        self.set_delivery_option(main_page)
        time.sleep(1)
        self.set_payment_method(main_page)
        time.sleep(1)
        order_status = self.check_order_status(main_page)
        time.sleep(1)
        self.assertEquals(order_status, "Oczekując na zatwierdzenie za pobraniem", "Order status doesn't match")

    def add_products_to_cart(self, main_page):
        main_page.click(MainPageLocators.CATEGORY_1)
        id_product_category_1 = 20
        for i in range(id_product_category_1, id_product_category_1 + 2):
            product_path = (
                By.XPATH,
                f'//section//div[@id=\"js-product-list\"]//div//article[@data-id-product=\"{str(i)}\"]//div//div[@class=\"thumbnail-top\"]')
            self.add_product(product_path, main_page)
        main_page.click(MainPageLocators.CATEGORY_2)
        id_product_category_2 = 199
        for i in range(id_product_category_2, id_product_category_2 + 6):
            product_path = (By.XPATH, f'//section//div[@id=\"js-product-list\"]//div//article[@data-id-product=\"{str(i)}\"]//div//div[@class=\"thumbnail-top\"]')
            self.add_product(product_path, main_page)

    @staticmethod
    def add_product(product_path, main_page):
        main_page.click(product_path)
        main_page.clear(MainPageLocators.WANTED_QUANTITY)
        main_page.back_space(MainPageLocators.WANTED_QUANTITY)
        main_page.set_value(MainPageLocators.WANTED_QUANTITY, random.randrange(1, 5))
        main_page.click(MainPageLocators.ADD_TO_CART)
        main_page.back()

    @staticmethod
    def delete_product_from_cart(main_page):
        main_page.driver.refresh()
        main_page.click(MainPageLocators.CART)
        products_inside_cart = main_page.driver.find_elements(*(
            By.CLASS_NAME, "cart-item"))
        remove = products_inside_cart[0].find_element(*(By.CLASS_NAME, "remove-from-cart"))
        remove.click()
        main_page.click(MainPageLocators.CREATE_ORDER)

    @staticmethod
    def register_user(main_page):
        # main_page.click(MainPageLocators.GO_LOGIN)
        # main_page.click(MainPageLocators.GO_REGISTER)
        main_page.click(MainPageLocators.GENDER_MEN)
        main_page.set_value(MainPageLocators.FIRSTNAME_INPUT, "test")
        main_page.set_value(MainPageLocators.LASTNAME_INPUT, "test")
        main_page.set_value(MainPageLocators.EMAIL_INPUT, f"test@test{random.random()}.com")
        main_page.set_value(MainPageLocators.PASSWORD_INPUT, "testtesttest1234")
        main_page.click(MainPageLocators.OPTION_1)
        main_page.click(MainPageLocators.OPTION_2)
        main_page.click(MainPageLocators.OPTION_3)
        main_page.click(MainPageLocators.OPTION_4)
        main_page.click(MainPageLocators.NEXT)

    @staticmethod
    def set_address(main_page):
        main_page.set_value(MainPageLocators.ADDRESS, "test")
        main_page.set_value(MainPageLocators.POST_CODE, "12-345")
        main_page.set_value(MainPageLocators.CITY, "Gdansk")
        main_page.click(MainPageLocators.CONFIRM_ADDRESSES)

    @staticmethod
    def set_delivery_option(main_page):
        main_page.click(MainPageLocators.DELIVERY_OPTION_1)     # set delivery option
        main_page.click(MainPageLocators.CONFIRM_DELIVERY_OPTION)

    @staticmethod
    def set_payment_method(main_page):
        main_page.click(MainPageLocators.PAYMENT_OPTION_2)
        main_page.click(MainPageLocators.CONDITIONS_TO_APPROVE)
        main_page.click(MainPageLocators.PAYMENT_CONFIRMATION)

    @staticmethod
    def check_order_status(main_page):
        main_page.click(MainPageLocators.GO_ACCOUNT)
        main_page.click(MainPageLocators.GO_HISTORY_AND_ORDER_DETAILS)
        main_page.click(MainPageLocators.GO_DETAILS)
        order_status = main_page.find_element(MainPageLocators.GET_STATUS)
        return order_status.text

    def tearDown(self):
        self.driver.close()
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
