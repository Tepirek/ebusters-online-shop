import time

from selenium.webdriver.common.by import By


class MainPageLocators(object):
    GO_BUTTON = (By.ID, 'submit')
    GO_LOGIN = (By.ID, '_desktop_user_info')
    GO_REGISTER = (By.CLASS_NAME, 'no-account')
    GENDER_MEN = (By.ID, 'field-id_gender-1')
    GENDER_WOMEN = (By.ID, 'field-id_gender-2')
    FIRSTNAME_INPUT = (By.ID, 'field-firstname')
    LASTNAME_INPUT = (By.ID, 'field-lastname')
    EMAIL_INPUT = (By.ID, 'field-email')
    PASSWORD_INPUT = (By.ID, 'field-password')
    OPTION_1 = (By.NAME, 'optin')
    OPTION_2 = (By.NAME, 'customer_privacy')
    OPTION_3 = (By.NAME, 'newsletter')
    OPTION_4 = (By.NAME, 'psgdpr')
    NEXT = (By.XPATH, '*//button[@class=\"continue btn btn-primary float-xs-right\"]')
    CATEGORY_1 = (By.ID, 'category-10')
    CATEGORY_2 = (By.ID, 'category-14')
    PRODUCT_1 = (By.XPATH, '//section//div[@id=\"js-product-list\"]//div//article[@data-id-product-attribute=\"0\"]')
    WANTED_QUANTITY = (By.ID, "quantity_wanted")
    ADD_TO_CART = (By.XPATH, "//div[@class=\"add\"]")
    CONTINUE_SHOPPING = (By.CLASS_NAME, "close")
    CREATE_ORDER = (By.XPATH, "*//div[@class=\"checkout cart-detailed-actions js-cart-detailed-actions card-block\"]")
    CART = (By.ID, "_desktop_cart")
    ADDRESS = (By.ID, "field-address1")
    POST_CODE = (By.ID, "field-postcode")
    CITY = (By.ID, "field-city")
    SAME_ADDRESS_TO_INVOICES = (By.ID, "use_same_address")
    CONFIRM_ADDRESSES = (By.NAME, "confirm-addresses")
    CONFIRM_DELIVERY_OPTION = (By.NAME, "confirmDeliveryOption")
    DELIVERY_OPTION_1 = (By.ID, "delivery_option_7")
    PAYMENT_OPTION_2 = (By.XPATH, "*//div[@id=\"payment-option-2-container\"]//label")
    CONDITIONS_TO_APPROVE = (By.ID, "conditions_to_approve[terms-and-conditions]")
    PAYMENT_CONFIRMATION = (By.XPATH, "*//div[@id=\"payment-confirmation\"]//div[@class=\"ps-shown-by-js\"]//button")
    GO_ACCOUNT = (By.XPATH, "*//a[@class=\"account\"]")
    GO_HISTORY_AND_ORDER_DETAILS = (By.ID, "history-link")
    GO_DETAILS = (By.XPATH, "*//a[@data-link-action=\"view-order-details\"]")
    GET_STATUS = (By.XPATH, "*//span[@class=\"label label-pill bright\"]")


