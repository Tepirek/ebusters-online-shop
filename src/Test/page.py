from selenium.webdriver import Keys
from selenium.webdriver.support.wait import WebDriverWait
from locators import MainPageLocators


class BasePage(object):
    def __init__(self, driver):
        self.driver = driver


class MainPage(BasePage):
    search_text_element = None

    def find_element(self, by):
        return WebDriverWait(self.driver, 100).until(lambda driver: driver.find_element(*by))

    def is_title_matches(self):
        return "Ebusters" in self.driver.title

    def back(self):
        self.driver.back()

    def click(self, by):
        element = self.find_element(by)
        element.click()

    def set_value(self, by, value):
        element = self.find_element(by)
        element.send_keys(value)

    def back_space(self, by):
        element = self.find_element(by)
        element.send_keys(Keys.BACK_SPACE)
    def clear(self, by):
        element = self.find_element(by)
        element.clear()


class SearchResultsPage(BasePage):

    def is_results_found(self):
        return "No results found." not in self.driver.page_source
