from prestapyt import PrestaShopWebServiceDict
import requests
import json
import re
import csv
WEBSERVICE_KEY = "QKTQJIB834C1PSIKAXAYI3KQHI2ZF58P"
def read_json(filename):
    with open(filename, "r", encoding='utf8') as jsonfile:
        ret = json.load(jsonfile)
    return ret


def create_attrs(id, value):
	return {"attrs":{"id":id}, "value": value}


def get_ids(presta, names, name):
    ids = []
    classes = presta.get(names)[names][name]
    for key in classes:
        ids.append(key["attrs"]["id"])
    return ids


def make_url_friendly(string):
	ret = re.sub(r"\s+", '-', string)
	ret = re.sub("[^0-9a-zA-Z]+", "-", ret)
	return ret.lower()


def remove_products(presta):
	ids = get_ids(presta, "products", "product")
	if len(ids):
		presta.delete("products", resource_ids=ids)


def remove_categories(presta):
	ids = get_ids(presta, "categories", "category")
	ids = [i for i in ids if int(i)>=3]
	if len(ids):
		presta.delete("categories", resource_ids=ids)


def add_to_categories(presta, template):
	return presta.add("categories", template)


def create_category_dict(data):
	categories = {}
	counter = 3
	for product in data:
		if not product["category"] in categories:
			categories[product["category"]] = {"id": counter, "parent": None}
			counter+=1
		if not product["subcategory"] in categories:
			categories[product["subcategory"]] = {"id": counter, "parent": categories[product["category"]]["id"]}
			counter += 1
	return categories


def create_categories(presta, data):
	categories = create_category_dict(data)
	for key, value in categories.items():
		template = read_json("./templates/categories.json")
		template["category"]["name"]["language"] = create_attrs("1", key)
		template["category"]["link_rewrite"]["language"] =  create_attrs("1", make_url_friendly(key))
		if value["parent"]:
			template["category"]["id_parent"] = id
			add_to_categories(presta, template)
		else:
			template["category"]["id_parent"] = "2"
			id = add_to_categories(presta, template)["prestashop"]["category"]["id"]
		categories[key]["id"] = id
	return categories


def create_products(presta, categories, products):
	template = read_json("templates/template_products.json")
	category = [{"id":"2"}]
	#categories = [{"id": "2"}] + [{"id": category_map[cd["id"]]} for cd in product["categories"]]
	# for product in products:
	# 	template["product"]["name"]["language"] = create_attrs("2", product["product_name"])
	# 	template["product"]["description"]["language"] = create_attrs("2", product["description"])
	# 	template["product"]["description_short"]["language"] = create_attrs("2", product["description"])
	# 	template["product"]["id_category_default"] = categories[product["subcategory"]]
	# 	template["product"]["price"] = product["price"]
	# 	template["product"]["associations"]["categories"]["category"] = category # categories  # product["categories"]
	presta.add("products", template)


def create_csv_products(product_data):
	product_data = clean_product_data(product_data)
	with open('./csv/products_import.csv', 'w', encoding='UTF8') as file:
		writer = csv.writer(file, delimiter =';', lineterminator=';\n')
		# header = ["Product ID", "Active (0/1)", "Name *", "Categories (x,y,z...)", "Price tax included", "Tax rules ID", "Wholesale price", "On sale (0/1)", "Discount amount",
		# 		  "Discount percent", "Discount from (yyyy-mm-dd)", "Discount to (yyyy-mm-dd)", "Reference #", "Supplier reference #", "Supplier", "Manufacturer", "EAN13",
		# 		  "UPC", "MPN", "Ecotax", "Width", "Height", "Depth","Weight", "Delivery time of in-stock products", "Delivery time of out-of-stock products with allowed orders",
		# 		  ]
		#writer.writerow(header)
		for index, product in enumerate(product_data, start=1):
			values_of_products = []
			values_of_products.append(index) 						#Product ID
			values_of_products.append("1") 							#Active (0/1)
			values_of_products.append(product["product_name"])		#Name *
			values_of_products.append("Home," + product["category"] +","+ product["subcategory"]) #Categories (x,y,z...)
			values_of_products.append(product["price"]) 			# "Price tax included
			values_of_products.append("1")							# "Tax rules ID
			values_of_products.append(product["price"] * 0.5) 		# Wholesale price
			if index in [1,3,5]:
				values_of_products.append("1")  # On sale (0/1)
				values_of_products.append("")  # Discount amount
				values_of_products.append("10")  # Discount percent
				values_of_products.append("2022-01-01")  # Discount from (yyyy-mm-dd)
				values_of_products.append("2024-01-01")  # Discount to (yyyy-mm-dd)
			else:
				values_of_products.append("0") 							#On sale (0/1)
				values_of_products.append("")							#Discount amount
				values_of_products.append("")							#Discount percent
				values_of_products.append("")							#Discount from (yyyy-mm-dd)
				values_of_products.append("")							#Discount to (yyyy-mm-dd)
			values_of_products.append(index)							#Reference #
			values_of_products.append("test")							#Supplier reference #
			values_of_products.append("best_supplier")					#Supplier
			values_of_products.append(product["manufacturer"])			#Manufacturer
			values_of_products.append("")								#EAN13
			values_of_products.append("")								#UPC
			values_of_products.append("")								#MPN
			values_of_products.append("1")								#Ecotax
			values_of_products.append("1")								#Width
			values_of_products.append("1")								#Height
			values_of_products.append("1")								#Depth
			values_of_products.append("1")								#Weight
			values_of_products.append("")								#Delivery time of in-stock products
			values_of_products.append("")								#Delivery time of out-of-stock products with allowed orders
			values_of_products.append("100")							#Quantity
			values_of_products.append("1")								#Minimal quantity
			values_of_products.append("0")								#Low stock level
			values_of_products.append("0")								#Send me an email when the quantity is under this level
			values_of_products.append("")								#Visibility
			values_of_products.append("")								#Additional shipping cost
			values_of_products.append("")								#Unity
			values_of_products.append("")								#Unit price
			values_of_products.append(product["description"][0:100])			#Summary
			values_of_products.append(product["description"])			#Description
			values_of_products.append("")								#Tags (x,y,z...)
			values_of_products.append("")								#Meta title
			values_of_products.append("")								#Meta keywords
			values_of_products.append("")								#Meta description
			values_of_products.append(make_url_friendly(product["product_name"]))			#URL rewritten
			values_of_products.append("W magazynie")					#Text when in stock
			values_of_products.append("Pozostało do zamówienie")		#Text when backorder allowed
			values_of_products.append("1")								#Available for order (0 = No, 1 = Yes)
			values_of_products.append("2022-01-01")						#Product available date
			values_of_products.append("2022-01-01")						#Product creation date
			values_of_products.append("1")								#Show price (0 = No, 1 = Yes)
			values_of_products.append(product["link"])					#Image URLs (x,y,z...)
			values_of_products.append("First alt text,Second alt text")	#Image alt texts (x,y,z...)
			values_of_products.append("0")								#Delete existing images (0 = No, 1 = Yes)
			values_of_products.append("Kolor:" + product["colour"] + ":1,Materiał:"+product["material"]+":2")	#Feature(Name:Value:Position)
			writer.writerow(values_of_products)
			if index > 505:
				break

def create_csv_combinations():
	products_ids = [2, 3, 4]
	attribute_name = "Zestaw z żarówką"
	attribute_values = ["Tak", "Nie"]
	with open('./csv/combinations_import.csv', 'w', encoding='UTF8') as file:
		writer = csv.writer(file, delimiter=';', lineterminator=';\n')
		#writer.writerow("header placeholder")
		for id in products_ids:
			for index, attribute_value in enumerate(attribute_values):
				combination = []
				combination.append(id) #Product ID*
				combination.append(id) #Reference #
				combination.append(attribute_name+":select:1") #Attribute (Name:Type:Position)*
				combination.append(attribute_value+":0") #Value (Value:Position)*
				combination.append("") #Supplier reference
				combination.append("") #Reference
				combination.append("") #EAN13
				combination.append("") #UPC
				combination.append("") #MPN
				combination.append("") #Wholesale price
				if index == 0 :
					combination.append("20") #Impact on price
				else:
					combination.append("0")  # Impact on price
				combination.append("0") #Ecotax
				combination.append("100") #Quantity
				combination.append("1") #Minimal quantity
				combination.append("0") #Low stock level
				combination.append("") #Impact on weight
				combination.append("") #Send me an email
				combination.append("0") #Default (0 = No, 1 = Yes)
				combination.append("2022-01-01") #Combination available date
				combination.append("") #Image position
				combination.append("") #Image URLs (x,y,z...)
				combination.append("First alt text,Second alt text") #Image alt texts (x,y,z...)
				combination.append("") #ID / Name of shop
				combination.append("0") #Advanced Stock Managment
				combination.append("0") #Depends on stock
				combination.append("") #Warehouse
				writer.writerow(combination)

def clean_product_data(product_data):
	for product in product_data:
		product["colour"] = product["colour"].replace(",", " ")
		product["material"] = product["material"].replace(",", " ")
	return product_data

def main():
	ses = requests.Session()
	ses.verify = False
	presta = PrestaShopWebServiceDict('http://localhost/api', WEBSERVICE_KEY)
	products_data = read_json("./templates/products.json")
	remove_categories(presta)
	categories = create_categories(presta, products_data)
	create_csv_products(products_data)
	create_csv_combinations()


if __name__ == '__main__':
	main()
