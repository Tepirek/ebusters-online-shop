from bs4 import BeautifulSoup
import requests
import pyodbc
import json

def get_categories(class_name):
	url = "https://www.lampy.pl"
	page = requests.get(url)
	soup = BeautifulSoup(page.content, 'html.parser')
	header_list = soup.findAll("li", class_=class_name)  # nazwa atrybutu dla listy głownych kategorii
	header_list = header_list[0:4] #4
	categories = []
	for category in header_list:
		category_name = category.find("span").getText().strip()
		subcategories = category.findAll("li",
										 class_="level1 nav-1-1 first parent type-0")  # nazwa atrybutu dla sub-kategorii
		subcategories = subcategories[0:3] #3
		for subcategory in subcategories:
			subcategory_name = subcategory.getText().strip()
			subcategory = subcategory.find('a', class_="level1")  # potrzebne żeby dostać tylko potrzebną sub-kategorię
			if subcategory is not None:
				subcategory.clear()
				if subcategory.get('href'):
					categories.append([category_name, subcategory_name,
									   subcategory['href']])  # zwracamy nazwę kategorii, sub i linka do subkategorii
	return categories


def get_products(url, class_name, cat_name, subcat_name):
	page = requests.get(url)
	soup = BeautifulSoup(page.content, 'html.parser')
	header_list = soup.findAll("li", class_=class_name)
	products = []
	for category in header_list:
		prod_id = category["data-sku"]
		description, colour, material = get_product_info(
			category.find("div", class_="image product-image").find("a")["href"])
		find_picture = category.find("div", class_="image product-image").find("picture").find("img")
		if find_picture.has_attr("data-src"):
			link = find_picture["data-src"]
		else:
			link = find_picture["src"]
		category = category.find("div", class_="info product-info")
		manufacturer = category.find('p', class_="product-manufacturer").text
		product_name = category.find('p', class_="product-name").find("span")["data-text"]
		price = float(category.find('div', class_="price-box")["data-price"].strip())
		product = {"category": cat_name,
				   "subcategory": subcat_name,
				   "product_name": product_name,
				   "id": prod_id,
				   "manufacturer": manufacturer,
				   "price": price,
				   "link": link,
				   "description": description,
				   "colour": colour,
				   "material": material}
		products.append(product)
	return products


def get_product_info(url):
	page = requests.get(url)
	soup = BeautifulSoup(page.content, 'html.parser')
	description = soup.find("div", class_="std")
	if description is not None:
		description = description.find("p").text.lstrip()
	else:
		description = ''
	table = soup.find("div", class_="toggle-block block-attributes open")
	if table is not None:
		colour = ''
		material = ''
		rows = table.findAll("tr")
		for row in rows:
			key = row.find("th").text.strip()
			if key == "Kolor":
				colour = row.find("td").text.strip()
			if key == "Materiał":
				material = row.find("td").getText().strip()
	return description, colour, material

def main():
	products = []
	sub_categories = get_categories("level0 nav-2 parent")
	for sub_category in sub_categories:
		products_to_add = get_products(sub_category[2], "product-card -landscape_sm", sub_category[0], sub_category[1])

		products += products_to_add
	for dictionairy in products:
		print(dictionairy)
	print(products)
	with open('./templates/products.json', 'w') as json_file:
		json.dump(products, json_file)

if __name__ == '__main__':
	main()
