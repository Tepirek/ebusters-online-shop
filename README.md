# eBusters Online Shop

## Instalacja

- Pobranie kodu źródłowego za pomocą jednej z poniższych komend:
    ```bash
    git clone git@gitlab.com:Tepirek/ebusters-online-shop.git
    ```
    ```bash
    git clone https://gitlab.com/Tepirek/ebusters-online-shop.git
    ```

- Stworzenie pliku konfiguracyjnego **.env**:
    ```bash
    cp .env.example .env
    ```

- Zbudować kontenery:
    ```bash
    docker compose up -d
    ```

- Sklep dostępny będzie pod adresem `localhost`.

---

## Dump bazy

- Uruchomienie skryptu:

```bash
docker compose exec database bash -c "cd db_dumps && ./dump.sh"
```

---

## Scrapper

- Uruchomienie skryptu:
  ```bash
  docker compose run --rm scrapper bash run.sh
  ```

---

## Test

- Uruchomienie testów `NOT WORKING YET`
   ```bash
   docker compose run --rm test bash run.sh
   ```
