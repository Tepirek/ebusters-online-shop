FROM python:3.10

WORKDIR /app

ENV VIRTUAL_ENV=/opt/venv

RUN python -m venv $VIRTUAL_ENV

ENV PATH="$VIRTUAL_ENV/bin:$PATH"

COPY /dockerfiles/test/requirements.txt /app

RUN pip install -r requirements.txt

COPY src/Test /app

RUN groupmod -g 1000 www-data && usermod -u 1000 -g 1000 www-data

RUN chown -R www-data:www-data /app