FROM mariadb:10.8.2

COPY database/docker-entrypoint-initdb.d /docker-entrypoint-initdb.d

COPY db_dumps /db_dumps
