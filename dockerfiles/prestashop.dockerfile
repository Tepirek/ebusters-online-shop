FROM prestashop/prestashop:1.7.8.7

RUN rm -rf /var/www/html/install

RUN apt-get update && apt-get install memcached libmemcached-dev zlib1g-dev -y

RUN pecl install memcached

RUN echo extension=memcached.so >> /usr/local/etc/php/php.ini

RUN /etc/init.d/apache2 restart

WORKDIR /etc/ssl/ebusters

COPY ssl/domains.ext .

COPY ssl/generate_keys.sh .

RUN ./generate_keys.sh

COPY apache2/default-ssl.conf /etc/apache2/sites-available/default-ssl.conf

RUN ln -s /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-enabled/default-ssl.conf

RUN a2enmod ssl

COPY config /tmp/init-scripts

WORKDIR /var/www/html

COPY app .

CMD ["/bin/sh", "entrypoint.sh"]
