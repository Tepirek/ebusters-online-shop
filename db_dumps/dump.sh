#!/bin/sh

DATE=$(date +'%d_%m_%Y_%H_%m_%S')

mysqldump \
  -u"$MARIADB_ROOT_USER" \
  -p"$MARIADB_ROOT_PASSWORD" \
  --opt \
  --all-databases \
  --flush-privileges \
  >dump_"$DATE".sql

chown "$USER_UID":"$USER_GID" dump_"$DATE".sql
